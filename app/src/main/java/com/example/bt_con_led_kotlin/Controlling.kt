package com.example.bt_con_led_kotlin

import android.app.Activity
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.InputStream
import java.util.*

class Controlling : Activity() {
    //koko
    private var mMaxChars = 50000 //Default//change this to string..........
    private var mDeviceUUID: UUID? = null
    private var mBTSocket: BluetoothSocket? = null
    private var mReadThread: ReadInput? = null
    private val mIsUserInitiatedDisconnect = false
    private var mIsBluetoothConnected = false
    private val mBtnDisconnect: Button? = null
    private var mDevice: BluetoothDevice? = null
    private var progressDialog: ProgressDialog? = null
    var btnOn: Button? = null
    var btnOff: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_controlling)
        ActivityHelper.initialize(this)
        // mBtnDisconnect = (Button) findViewById(R.id.btnDisconnect);
        btnOn = findViewById<View>(R.id.on) as Button
        btnOff = findViewById<View>(R.id.off) as Button
        val intent = intent
        val b = intent.extras
        mDevice = b!!.getParcelable(MainActivity.DEVICE_EXTRA)
        mDeviceUUID = UUID.fromString(b.getString(MainActivity.DEVICE_UUID))
        mMaxChars = b.getInt(MainActivity.BUFFER_SIZE)
        Log.d(TAG, "Ready")
        btnOn!!.setOnClickListener {
            // TODO Auto-generated method stub
            try {
                mBTSocket!!.outputStream.write(on.toByteArray())
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
        btnOff!!.setOnClickListener {
            // TODO Auto-generated method stub
            try {
                mBTSocket!!.outputStream.write(off.toByteArray())
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
    }

    private inner class ReadInput : Runnable {
        private var bStop = false
        private val t: Thread
        val isRunning: Boolean
            get() = t.isAlive

        override fun run() {
            val inputStream: InputStream
            try {
                inputStream = mBTSocket!!.inputStream
                while (!bStop) {
                    val buffer = ByteArray(256)
                    if (inputStream.available() > 0) {
                        inputStream.read(buffer)
                        var i = 0
                        /*
                         * This is needed because new String(buffer) is taking the entire buffer i.e. 256 chars on Android 2.3.4 http://stackoverflow.com/a/8843462/1287554
                         */i = 0
                        while (i < buffer.size && buffer[i] != 0) {
                            i++
                        }
                        val strInput = String(buffer, 0, i)

                        /*
                         * If checked then receive text, better design would probably be to stop thread if unchecked and free resources, but this is a quick fix
                         */
                    }
                    Thread.sleep(500)
                }
            } catch (e: IOException) {
// TODO Auto-generated catch block
                e.printStackTrace()
            } catch (e: InterruptedException) {
// TODO Auto-generated catch block
                e.printStackTrace()
            }
        }

        fun stop() {
            bStop = true
        }

        init {
            t = Thread(this, "Input Thread")
            t.start()
        }
    }

    private inner class DisConnectBT : AsyncTask<Void?, Void?, Void?>() {
        override fun onPreExecute() {}
         override fun doInBackground(vararg p0: Void?): Void? { //cant inderstand these dotss
            if (mReadThread != null) {
                mReadThread!!.stop()
                while (mReadThread!!.isRunning);
                mReadThread = null
            }
            try {
                mBTSocket!!.close()
            } catch (e: IOException) {
// TODO Auto-generated catch block
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mIsBluetoothConnected = false
            if (mIsUserInitiatedDisconnect) {
                finish()
            }
        }
    }

    private fun msg(s: String) {
        Toast.makeText(applicationContext, s, Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        if (mBTSocket != null && mIsBluetoothConnected) {
            DisConnectBT().execute()
        }
        Log.d(TAG, "Paused")
        super.onPause()
    }

    override fun onResume() {
        if (mBTSocket == null || !mIsBluetoothConnected) {
            ConnectBT().execute()
        }
        Log.d(TAG, "Resumed")
        super.onResume()
    }

    override fun onStop() {
        Log.d(TAG, "Stopped")
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
// TODO Auto-generated method stub
        super.onSaveInstanceState(outState)
    }

    private abstract inner class ConnectBT : AsyncTask<Void?, Void?, Void?>() {
        private var mConnectSuccessful = true
        override fun onPreExecute() {
            progressDialog = ProgressDialog.show(this@Controlling, "Hold on", "Connecting") // http://stackoverflow.com/a/11130220/1287554
        }

        protected fun doInBackground(vararg devices: Void): Void? {
            try {
                if (mBTSocket == null || !mIsBluetoothConnected) {
                    mBTSocket = mDevice!!.createInsecureRfcommSocketToServiceRecord(mDeviceUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    //mBTSocket.connect()
                }
            } catch (e: IOException) {
// Unable to connect to device`
                // e.printStackTrace();
                mConnectSuccessful = false
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (!mConnectSuccessful) {
                Toast.makeText(applicationContext, "Could not connect to device.Please turn on your Hardware", Toast.LENGTH_LONG).show()
                finish()
            } else {
                msg("Connected to device")
                mIsBluetoothConnected = true
                mReadThread = ReadInput() // Kick off input reader
            }
            progressDialog!!.dismiss()
        }
    }

    override fun onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy()
    }

    companion object {
        private const val TAG = "BlueTest5-Controlling"
        const val on = "92" //on
        const val off = "79" //off
    }
}